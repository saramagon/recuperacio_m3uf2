import java.util.ArrayList;
import java.util.Scanner;

/*
 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
 * caselles que inicialment està buit.
 * 
 * @author Sara-intentant-ho
 * 
 * @versio 7/06/20 2
 * 
 */

public class Conecta4_2 {

	/*
	 * l'enunciat demana no fer variables estàtiques, però he decidit fer aquestes:
	 * Variables globals,
	 * 
	 * el taulell per a modificar-lo directament al cridar-lo des de qualsevol de
	 * les funcions
	 */
	static int[][] taulell = new int[4][4];

	/*
	 * Un escaner per recollir tant les opcions com els noms és més lògic fer-lo
	 * static, per no haver d'anar declarant-lo cada cop que el necessitem.
	 */
	static Scanner sc = new Scanner(System.in);

	/* Els jugadors, per a controlar-los millor */
	static String jugador1 = " ";
	static String jugador2 = " ";

	public static void main(String[] args) {

		/*
		 * VARIABLES locals
		 * 
		 * per al control de torn
		 */
		int torn = 0;

		/* control fi partida */
		boolean fi = false;

		/*
		 * Variables per al nombre de files i columnes i el tauler per jugar
		 */
		int fil = 0;
		int col = 0;
		int[][] tauler = new int[fil][col];

		/* Una array per emmagatzemar els guanyadors */
		ArrayList<String> ordre = new ArrayList<String>();

		/* Declarem una variable per a la tria de l'opció */
		int op = -1;

		/* Fem un bucle per a iniciar el joc */
		while (op != 0) {

			op = menu();
			int triar = sc.nextInt();
			switch (triar) {
			case 1:
				op = mostrarAjuda();
				break;
			case 2:
				op = opcions();
				break;
			case 3:
				op = jugar(torn);
				break;
			case 4:
				op = ranking();
				break;
			case 0:
				op = sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}
		}

	}

	private static int ranking() {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return -1, per fer sortir del bucle principal i finalitzar el joc
	 */
	private static int sortir() {
		System.out.println("Pleguem!!");
		return 0;

	}

	private static int jugar(int torn) {
		int col = demanarColumna();
		posarFitxa(col, torn);
		return 0;
	}

	/*
	 * Menú, apareixerà sempre que no es seleccioni 0, perquè va a parar al main
	 * 
	 * @return op: per les opcions del menu que es retorna al main
	 */
	private static int menu() {
		int op = sc.nextInt();

		while (op != -1) {
			System.out.println("=======MENú=======");
			System.out.println("   1 - Ajuda");
			System.out.println("   2 - Opcions");
			System.out.println("   3 - Jugar partida");
			System.out.println("   4 - Veure rankings");
			System.out.println("   0 - Sortir");

		}
		return op;
	}

	/*
	 * Funció per recollir els noms dels jugadors de la partida i poder donar el
	 * ranking
	 * 
	 * @return op: per a retornar-lo al main i que continui el joc
	 */
	private static int opcions() {
		int op = -1;
		sc.nextLine();
		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a");
			jugador1 = sc.nextLine();
			jugador2 = "maquina";
		} else
			System.out.println("Nom dels/de les jugadors/es");
		jugador1 = sc.nextLine();
		jugador2 = sc.nextLine();

		return op;
	}

	/*
	 * Mostra l'ajuda o instruccions
	 * 
	 * @return op: per a continuar l'execució, mostrar el menú per a continuar el
	 * joc
	 */
	private static int mostrarAjuda() {
		int op = -1;
		String salt = System.getProperty("line.separator");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);
		return op;
	}

	/*
	 * Per a controlar els torns
	 * 
	 * @param torn: és el torn que està jugant i comprovar el canvi de jugadors
	 * 
	 * @return torn: per al torn següent i canvi de jugador
	 */
	private static int canviTorn(int torn) {
		if (torn == 1) {
			torn = 2;
		} else {
			torn = 1;
		}
		return torn;
	}

	/*
	 * Col·locació de la fitxa del jugador del torn corresponent
	 * 
	 * @param c: columna on es col·locarà la fitxa
	 * 
	 * @ param torn: per controlar de quin jugador és la tirada
	 */
	private static void posarFitxa(int c, int torn) {
		for (int i = 0; i < taulell.length; i++) {
			if (taulell[i][c] != 0) {
				taulell[i - 1][c] = torn;
			}
		}
		partidaTaulell(taulell);
	}

	/*
	 * Sol·licitem la columna on vol col·locar la fitxa el jugador del torn
	 * corresponent
	 * 
	 * @return col: és la columna, per a que la funció posarFitxa sàpiga on l'ha de
	 * col·locar.
	 */
	private static int demanarColumna() {
		int col = 0;
		System.out.print("On poses la fitxa?");
		col = sc.nextInt();
		return col;
	}

	/*
	 * Dibuixar el tauler on s'ha de jugar inicialitzat a 0
	 * 
	 * @param fil: per al número de files triat o estàndard
	 * 
	 * @param col: per al número de columnes triat o estàndard
	 * 
	 * @param tauler: on es jugagarà, amb una mida de fil X col
	 */
	private static void taulellInici(int fil, int col, int[][] taulell) {
		for (int i = 0; i < fil; i++) {
			for (int j = 0; j < col; j++) {
				System.out.println(taulell[i][j] = 0); // inizialitzat a 0 per mostrar que és buit
			}
		}
	}

	/*
	 * Per anar dibuixant l'evolució de la partida, haurem de comprovar jugador i
	 * columnes, files o diagonals, per anar posant les fitxes...
	 * 
	 * @param taulell: per anar veient on estan les fitxes dels jugadors.
	 */
	public static void partidaTaulell(int[][] taulell) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");

	}

	/*
	 * Anar verificant abans del canvi de torn que el jugador del torn corresponent
	 * no hagi col·locat les fitxes en linia
	 */
	private static boolean comprovar() {
		int j1 = 0; // variable local per comptar les fitxes del jugador1
		int j2 = 0; // variable local per comptar les fitxes del jugador2

		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				if (taulell[i][j] == 1) {
					j1++;
				} else if (taulell[i][j] == 2) {
					j2++;
				} else {

				}
			}
		}
		if (j1 > j2) {
			System.out.println("A guanyat " + jugador1);
		} else if (j2 > j1) {
			System.out.println("A guanyat " + jugador2);
		} else if (j1 == j2) {
			System.out.println("empat!!");
		} else {
			System.out.println("seguim jugant...");
		}
		return false;
	}

}