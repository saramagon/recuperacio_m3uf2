import java.util.Scanner;

public class Enlinea4 {

	static int f = 6;
	static int c = 7;
	static int[][] taulell = new int[f][c];

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String jugador1 = " ";
		String jugador2 = " ";

		int op = 0;

		while (op != 0) {

			mostrarMenu();

			ajuda();
			configurar();
			midaTaulell();
			nomsJugadors();
			jugar();
			comprovarTorn();
			tirarFitxa();
			fiPartida();
			ranking();
			op = sortir(op);
		}

	}

	/**
	 * Per mostrar menú amb les opcions que es poden seleccionar
	 * 
	 * @return op: és la selecció que accionarà cada opció
	 */
	private static void mostrarMenu() {
		int op = 0;
		System.out.println("=======MENú=======");
		System.out.println("   1 - Ajuda");
		System.out.println("   2 - Opcions");
		System.out.println("   3 - Jugar partida");
		System.out.println("   4 - Veure rankings");
		System.out.println("   0 - Sortir");
		seleccioMenu(op);

	}

	private static void seleccioMenu(int op) {
		int torn = 1;
		boolean win = false;
		while (op != 0) {

			op = sc.nextInt();
			switch (op) {
			case 1:
				mostrarAjuda();
				break;
			case 2:
				configuracio(taulell);
				break;
			case 3:
				op= jugar(torn, win);
				break;
			case 4:
				ranking();
				break;
			case 0:
				sortir(op);
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}
		}

	}

	/**
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return op, per fer sortir del bucle principal i tancar el joc
	 */
	private static int sortir(int op) {
		System.out.println("Pleguem!, Na Noch!!");
		op = 0;
		return op;
	}

	public static void dibuixaPartida(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");

	}

	private static int jugar(int torn, boolean win) {
		String jugador1 = "J";
		String jugador2 = "E";
		 int columna= 0;
		if (torn == 1) {
			System.out.println("En quina columna? " + jugador1);
			columna = sc.nextInt();

			// comprovem que està dins del rang del taulell
			if (columna < 0 || columna >= c) {
				System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
			} else {
				for (int i = 0; i < taulell.length; i++) {
					if (taulell[i][columna] != 0) {
						taulell[i - 1][columna] = torn;
					}

				}
			}
		} else {

			System.out.println("En quina columna?" + jugador2);
			columna = sc.nextInt();

			// comprovem que està dins del rang del taulell
			if (columna < 0 || columna >= c) {
				System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
			} else {
				for (int i = 0; i < taulell.length; i++) {

					if (taulell[i][columna] != 0) {
						taulell[i - 1][columna] = torn;
					}
				}
			}
		}
		return op;
	}



	private static int comprovarTorn(int torn) {
		if (torn == 1) {
			torn = 2;
			return torn;
		} else {
			torn = 1;
			return torn;
		}
	}

}
