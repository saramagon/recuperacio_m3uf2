import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
 * caselles que inicialment està buit.
 * 
 * @author Sara-intentant-ho
 * 
 *         iniciat el 04/06/20
 * @versio 5.3
 * 
 */

public class Taulell_4 {

	/**
	 * Variables globals, les hem de cridar des de totes les funcions i axí no les
	 * hem de declarar totes les vegades i estalviem memòria
	 */
	/* Variables per als jugadors */
	static String jugador1 = null;
	static String jugador2 = null;
	static ArrayList<String> classifList = new ArrayList<String>();

	/* Variables per al taulell de joc */
	public static int f = 6;
	public static int c = 7;
	public static int[][] taulell = new int[f][c];

	public static void main(String[] args) {

		int torn = 0;
		boolean win = false;
		int op = -1;
		Scanner sc = new Scanner(System.in);

		while (op != 0) {
			System.out.println("=======MENú=======");
			System.out.println("   1 - Ajuda");
			System.out.println("   2 - Opcions");
			System.out.println("   3 - Jugar partida");
			System.out.println("   4 - Veure rankings");
			System.out.println("   0 - Sortir");

			int triar = sc.nextInt();
			switch (triar) {
			case 1:
				op = mostrarajuda();
				break;
			case 2:
				op = configuracio(taulell);
				break;
			case 3:
				op = jugar(torn, win);
				break;
			case 4:
				op = ranking(torn);
				break;
			case 0:
				op = sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}

		}

	}

	/**
	 * Per jugar necessitem el tauler, cridem la funció per a dibuixar el tauler,
	 * amb el núm. columnes, núm. de files de les opcions del jugador. Comprovem si
	 * hi ha guanyador per finalitzar la partida. Tirada de les fitxes segons el
	 * torn de jugador.
	 * 
	 * @param torn: per a iniciar el joc amb el torn 1 i controlar els canvis de
	 *              torn
	 * 
	 * @param win:  per controlar el final de la partida
	 * 
	 * @return -1: per tornar al menú principal ¿?
	 * 
	 */
	private static int jugar(int torn, boolean win) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Comencem la partida...");
		dibuixaPartida(taulell);
		torn = comprovarTorn(torn); // canvi de torn
		
		int columna = 0;

		while (!win) {

			boolean coloca;
			do {
				int columna1 = triarColumna(torn);
				coloca = colocarFitxa(torn, columna1);
			} while (!coloca);
			win = fiPartida(torn);
			torn = comprovarTorn(torn); // canvi de torn
			dibuixaPartida(taulell);
		}
		return -1;
	}

	/**
	 * Seleccionar la columna i comprovar que està dins de la mida del taulell
	 * 
	 * @param torn: per saber el jugador que juga
	 * @return columna: per saber a quina columna es colocarà la fitxa
	 */
	private static int triarColumna(int torn1) {
		Scanner sc = new Scanner(System.in);
		int columna;
		if (torn1 == 1) {
			System.out.println("En quina columna? " + jugador1);
			columna = sc.nextInt();

			// comprovem que està dins del rang del taulell
			if (columna < 0 || columna >= c) {
				System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
			} else {
				return columna;
			}
		} else {

			System.out.println("En quina columna?" + jugador2);
			columna = sc.nextInt();

			// comprovem que està dins del rang del taulell
			if (columna < 0 || columna >= c) {
				System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
			} else {
				return columna;
			}
		}
		return -1;
	}

	/**
	 * Col·locar la fitxa del jugador, la fitxa cau fins a la última fila, en la
	 * columna seleccionada si no està plena per una altra fitxa, les fitxes es
	 * corresponen amb el torn, jugador1 = 1 i jugador2 =2
	 * 
	 * @param torn:    per controlar el jugador i la fitxa que es col·loca
	 * 
	 * @param columna: per saber en quina columna
	 * 
	 * @return boolean: per controlar si s'ha col·locat
	 */
	private static boolean colocarFitxa(int torn, int columna) {

		if (columna == -1) {
			System.out.println("Alguna cosa no va bé.... ¿? ");
		} else {
			for (int i = 0; i < taulell.length; i++) {
				if ((i == 0) && (taulell[i][columna] != 0)) {
					System.out.println("columna completa!");
					torn = comprovarTorn(torn);
					return true;
				} else if (taulell[i][columna] != 0) {
					taulell[i - 1][columna] = torn;
					System.out.println("ja hi ha una fitxa");
					return false;
				} else if (((i + 1) == f) && (taulell[i][columna] == 0)) {
					taulell[i][columna] = torn;
					System.out.println("fins a baix");
					return false;
				} else if ((i == 0) && (taulell[i][columna] != 0)) {
					System.out.println("columna completa!");
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Funció iniciar i controlar els torns
	 * 
	 * @param torn: si s'ha d'iniciar el torn o quin torn estava jugant
	 * @return torn: per continuar el joc
	 */
	private static int comprovarTorn(int torn) {
		if (torn == 0) {
			torn = 1;
			return torn;
		} else if (torn == 1) {
			torn = 2;
			return torn;
		} else {
			torn = 1;
			return torn;
		}
	}

	/**
	 * Aquí comprovarem si hi ha guanyador de la partida, cal comprovar les files
	 * horitzontals, verticals i diagonals.
	 * 
	 * @param torn: per saber quin jugador està tirant i fer les comprovacions de
	 *              guanyador
	 * 
	 * @return win: boleà per saber si hi ha guanyador i qui ha sigut
	 */
	private static boolean fiPartida(int torn) {
		// variables per comptar les fitxes
		int conn = 0;

		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell.length; j++) {
				if (taulell[i][j] == torn) {
					if ((torn == 1) && (conn == 4)) {
						conn++;
						System.out.println("A guanyat el jugador " + torn);
						classifList.add(jugador1);
						return true;

					} else if ((torn == 2) && (conn == 4)) {
						conn++;
						System.out.println("A guanyat el jugador " + torn);
						classifList.add(jugador2);
						return true;
					}
				}
			}
		}
		// comprovar verticals
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell.length; j++) {
				if (taulell[j][i] == torn) {
					if ((torn == 1) && (conn == 4)) {
						conn++;
						System.out.println("A guanyat el jugador " + torn);
						classifList.add(jugador1);
						return true;
					} else if ((torn == 2) && (conn == 4)) {
						conn++;
						System.out.println("A guanyat el jugador " + torn);
						classifList.add(jugador2);
						return true;
					}
				}
			}
		}
		// comprovar diagonals
		for (int i = 0; i < taulell.length; i++) {
			if (taulell[i][i] == torn) {
				if ((torn == 1) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador1);
					return true;
				} else if ((torn == 2) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador2);
					return true;
				}
			} else if (taulell[i][(taulell.length - 1) - i] == torn) {

				if ((torn == 1) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador1);
					return true;
				} else if ((torn == 2) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador2);
					return true;
				}
			} else if (taulell[(taulell.length - 1) - i][i] == torn) {

				if ((torn == 1) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador1);
					return true;
				} else if ((torn == 2) && (conn == 4)) {
					conn++;
					System.out.println("A guanyat el jugador " + torn);
					classifList.add(jugador2);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Dibuixar el tauler on s'ha de jugar inicialitzat a 0
	 * 
	 */
	private static void taulellIni() {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				taulell[i][j] = 0;
			}
		}
	}

	/**
	 * Per dibuixar el taulell a l'inici i durant la partida
	 * 
	 * @param taulell: el taulell que hi ha guardat en cada moment
	 */
	public static void dibuixaPartida(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");

	}

	/**
	 * Configuració del joc, triar jugadors i des d'aquí, seleccionar la mida del
	 * taulell
	 * 
	 * @param taulell: per a configurar la mida, sigui la estàndard o la nova mida
	 *                 seleccionada
	 * @return op: per tornar al menú principal i seleccionar altres opcions
	 */
	private static int configuracio(int[][] taulell2) {
		int op = -1;
		Scanner sc = new Scanner(System.in);
		sc.nextLine(); // per evitar errors de lectura
		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		jug = sc.nextInt();
		sc.nextLine();
		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a"); // amb aquesta opció hi haurà la màquina
			jugador1 = sc.nextLine();
			System.out.println("jugador1 és " + jugador1);
			jugador2 = "maquina";
			System.out.println("jugador2 és " + jugador2);
			midaTaulell(taulell); // cridem la funció per seleccionar la mida del taulell
			return op;

		} else {
			System.out.println("Nom dels/de les jugadors/es"); // jugaran dos humans...
			jugador1 = sc.nextLine();
			jugador2 = sc.nextLine();
			System.out.println("La partida es juga entre " + jugador1 + " i el " + jugador2);
			midaTaulell(taulell); // cridem la funció per seleccionar la mida del taulell
			return op;
		}
	}

	/**
	 * Seleccionar la mida del taulell, funció activada des de la configuració del
	 * joc.
	 * 
	 * @param taulell: per a modificar el taulell si el jugador selecciona una mida
	 *                 diferent a la estàndard
	 * 
	 */
	private static void midaTaulell(int[][] taulell2) {
		int mida = 0; // variable local per a saber si vol modificar la mida del taulell
		Scanner sc = new Scanner(System.in);
		sc.nextLine(); // netejar l'escaner per evitar errors.
		System.out
				.println("Tria la mida del taulell presionant qualsevol" + "número, si no, presiona 0 i serà de 6 X 7");
		mida = sc.nextInt();
		sc.nextLine();
		if (mida != 0) {
			System.out.println("Quantitat de files, mínim 4");
			int fil = sc.nextInt();
			f = fil;
			sc.nextLine();
			System.out.println("Quantitat de columnes, mínim 4");
			int col = sc.nextInt();
			c = col;
			sc.nextLine();
			System.out.println("El taulell serà de " + f + " files " + c + " columnes.");
			dibuixaPartida(taulell);
		} else {
			f = 6;
			c = 7;
			System.out.println("El taulell serà de " + f + " files " + c + " columnes.");
			taulellIni();
		}
	}

	/**
	 * Per emmagatzemar els guanyadors i controlar-ne la posició
	 *
	 * @param torn:per saber quin jugdor a guanyat i s'ha d'afegir a la llista de
	 *                 classificació
	 * @return -1: per a tornar al menú per seguir o sortir del joc
	 */
	private static int ranking(int torn) {
		if (torn == 1) {
			if (classifList.contains(jugador1)) {
				for (int i = 0; i < classifList.size(); i++) {
					System.out.println("Classificació");
					System.out.println((jugador1) + (Collections.frequency(classifList, jugador1)));
				}
			} else if (torn == 2) {
				if (classifList.contains(jugador2)) {
					for (int i = 0; i < classifList.size(); i++) {
						System.out.println("Classificació");
						System.out.println((jugador2) + (Collections.frequency(classifList, jugador2)));
					}
				}
			}
		}
		return -1;
	}

	/**
	 * Funicó per mostrar l'ajuda, s'intenta separar amb salts de linea (no s'ha
	 * aconseguit)
	 * 
	 * @return op: per retornar al menú inicial y triar una nova opció
	 */
	private static int mostrarajuda() {
		int op = -1;
		String salt = System.getProperty("line.separaor");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);
		return op;

	}

	/**
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return 0: per fer sortir del bucle principal i finalitzar el joc
	 */
	private static int sortir() {
		System.out.println("Pleguem!, Na Noch!!");
		return 0;
	}

}
