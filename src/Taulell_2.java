import java.util.Scanner;

/*
 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
 * caselles que inicialment està buit.
 * 
 * @author Sara-intentant-ho
 * 
 * iniciat el 04/06/20
 * @versio 9/06/20 5.1
 * 
 */

public class Taulell_2 {

	static String jugador1 = null;
	static String jugador2 = null;
	static int f = 6;
	static int c = 7;
	static int[][] taulell = new int[f][c];

	public static void main(String[] args) {
		int torn = 1;
		boolean win = false;
		int op = -1;
		Scanner sc = new Scanner(System.in);

		while (op != 0) {
			System.out.println("=======MENú=======");
			System.out.println("   1 - Ajuda");
			System.out.println("   2 - Opcions");
			System.out.println("   3 - Jugar partida");
			System.out.println("   4 - Veure rankings");
			System.out.println("   0 - Sortir");

			int triar = sc.nextInt();
			switch (triar) {
			case 1:
				op = mostrarajuda();
				break;
			case 2:
				op = configuracio(taulell);
				break;
			case 3:
				op = jugar(torn, win);
				break;
			case 4:
				op = ranking();
				break;
			case 0:
				op = sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}

		}

	}

	private static int jugar(int torn, boolean win) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Comencem la partida...");

		dibuixaPartida(taulell);
		int columna = 0;

		while (win == false) {
			comprovarTorn(torn); // canvi de torn
			win = fiPartida(torn, taulell);
			if (torn != 0) {
				if (torn == 1) {
					System.out.println("En quina columna? " + jugador1);
					columna = sc.nextInt();
					// comprovem que està dins del rang del taulell
					if (columna < 0 || columna >= c) {
						System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
					} else {
						// comprovem fins on ha de caure
						for (int i = 0; i < taulell.length; i++) {
							if (taulell[i][columna] != 0) {
								taulell[i - 1][columna] = torn;
							}
						}
						dibuixaPartida(taulell);
					}
				}
			} else {
				System.out.println("En quina columna?" + jugador2);
				columna = sc.nextInt();
				// comprovem que està dins del rang del taulell
				if (columna < 0 || columna >= c) {
					System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
				} else {
					// comprovem fins on ha de caure
					for (int i = 0; i < taulell.length; i++) {
						if (taulell[i][columna] != 0) {
							taulell[i - 1][columna] = torn;
						}
					}
					dibuixaPartida(taulell);
				}
			}
		}

		return 0;
	}

	
	/*
	 * Aquí comprovarem si hi ha guanyador de la partida, cal comprovar les files
	 * horitzontals, verticals i diagonals.
	 * 
	 * @return win: boleà per saber si hi ha guanyador i qui ha sigut
	 */
	private static boolean finPartida(int torn, boolean win) {
		if(win) {
			System.out.println("Ha guanyat "+noms[turno-1]);
			return true;
		}else {
			boolean flag = true;
			for (int i = 0; i < taulell.length; i++) {
				for (int j = 0; j < taulell[0].length; j++) {
					if(taulell[i][j]==0) {
						flag = false;
					}
				}
			}
			if(flag==true) {
				System.out.println("TAULELL PLE. EMPAT");
				return true;
			}else {
				return false;
			}
		}
	}

	private static boolean comprobarLinea(int torn) {
		return lineaVert(torn) || lineaHoritz(torn) || lineaDiag(torn); 
	}

	private static boolean lineaHoritz(int torn) {
		for (int i = 0; i < taulell.length; i++) {
			int cont = 0;
			for (int j = 0; j < taulell[0].length; j++) {
				if(taulell[i][j]==torn) {
					cont++;
				}
			}
			if(cont==4) {
				return true;
			}
			
		}
		return false;
	}

	private static boolean lineaVert(int torn) {
		for (int i = 0; i < taulell.length; i++) {
			int cont = 0;
			for (int j = 0; j < taulell[0].length; j++) {
				if(taulell[j][i]==torn) {
					cont++;
				}
			}
			if(cont==4) {
				return true;
			}
			
		}
		return false;
	}

	private static boolean lineaDiag(int torn) {
		int cont = 0;
		for (int i = 0; i < 4; i++) {
			if(taulell[i][i]==torn) {
				cont++;
			}
		}
		if(cont==4) {
			return true;
		}
		cont = 0;
		for (int i = 0; i < 4; i++) {
			if(taulell[3-i][i]==torn) {
				cont++;
			}
		}
		if(cont==4) {
			return true;
		}
		return false;
	}

	/*
	 * private static void colocarFitxa(int torn, int columna) { for (int i = 0; i <
	 * taulell.length; i++) { if (taulell[i][columna] != 0) { taulell[i -
	 * 1][columna] = torn; } } dibuixaPartida(taulell); }
	 */

	private static int comprovarTorn(int torn) {
		if (torn == 1) {
			torn = 2;
			return torn;
		} else {
			torn = 1;
			return torn;
		}
	}

	private static void taulellInici(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				taulell[i][j] = 0;
			}
		}
	}

	public static void dibuixaPartida(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");

	}

	private static int configuracio(int[][] taulell2) {
		int op = -1;
		Scanner sc = new Scanner(System.in);
		sc.nextLine(); // per evitar errors de lectura
		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		jug = sc.nextInt();
		sc.nextLine(); 
		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a"); // amb aquesta opció hi haurà la màquina
			jugador1 = sc.nextLine();
			System.out.println("jugador1 és " + jugador1);
			jugador2 = "maquina";
			System.out.println("jugador2 és " + jugador2);
			midaTaulell(); // cridem la funció per seleccionar la mida del taulell
			return op;

		} else {
			System.out.println("Nom dels/de les jugadors/es"); // jugaran dos humans...
			jugador1 = sc.nextLine();
			jugador2 = sc.nextLine();
			System.out.println("La partida es juga entre " + jugador1 + "i el " + jugador2);
			midaTaulell(); // cridem la funció per seleccionar la mida del taulell
			return op;
		}
	}

	private static void midaTaulell() {
		int mida = 0; // variable local per a saber si vol modificar la mida del taulell
		Scanner sc = new Scanner(System.in);
		sc.nextLine(); // netejar l'escaner per evitar errors.
		System.out
				.println("Tria la mida del taulell presionant qualsevol" + "número, si no, presiona 0 i serà de 6 X 7");
		mida = sc.nextInt();
		sc.nextLine();
		if (mida != 0) {
			System.out.println("Quantitat de columnes, mínim 4");
			c = sc.nextInt();
			sc.nextLine();
			System.out.println("Quantitat de columnes, mínim 4");
			f = sc.nextInt();
			sc.nextLine();
			System.out.println("El taulell serà de " + c + " columnes " + f + " files.");
		} else {
			c = 6;
			f = 7;
			System.out.println("El taulell serà de " + c + " columnes " + f + " files.");
		}
	}

	/*
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return -1, per fer sortir del bucle principal i finalitzar el joc
	 */
	private static int sortir() {
		System.out.println("Pleguem!, Na Noch!!");
		return 0;
	}

	/* Per emmagatzemar els guanyadors i controlar-ne la posició */
	private static int ranking() {
		return -1;
		// TODO tot el que ha de fer..., controlar nom, puntuació i ordre ¿?

	}

	/* Funicó per mostrar l'ajuda, s'intenta separar amb salts de linea */
	private static int mostrarajuda() {
		int op = -1;
		String salt = System.getProperty("line.separaor");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);
		return op;

	}

}
