import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
 * caselles que inicialment està buit.
 * 
 * @author Sara-intentant-ho
 * 
 * 
 * @version 6.2 - iniciat el 04/06/20
 * 
 */

public class Taulell {

	/**
	 * Variables globals, les hem de cridar des de totes les funcions i axí no les
	 * hem de declarar totes les vegades i estalviem memòria
	 */
	/* Variables per als jugadors */
	static String jugador1 = null;
	static String jugador2 = null;
	static TreeMap<String, Integer> classifList = new TreeMap<>(); // HashMap pegate un tiro, Lucas m'ha recomenat el TreeMap

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		int torn = 0;
		int op = -1;

		int[] tamanyTaulell = { 6, 7 };
		while (op != 0) {
			System.out.println("=======MENú=======");
			System.out.println("   1 - Ajuda");
			System.out.println("   2 - Opcions");
			System.out.println("   3 - Jugar partida");
			System.out.println("   4 - Veure rankings");
			System.out.println("   0 - Sortir");

			op = sc.nextInt();
			switch (op) {
			case 1:
				mostrarajuda();
				break;
			case 2:
				tamanyTaulell = configuracio();
				break;
			case 3:
				jugar(tamanyTaulell);
				break;
			case 4:
				ranking();
				break;
			case 0:
				sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}

		}

	}

	/**
	 * Per jugar necessitem el tauler, cridem la funció per a dibuixar el tauler,
	 * amb el núm. columnes, núm. de files de les opcions del jugador. Comprovem si
	 * hi ha guanyador per finalitzar la partida. Tirada de les fitxes segons el
	 * torn de jugador.
	 * 
	 * @param tamanyTaulell: definir alçada i amplada del taulell
	 * 
	 */
	public static void jugar(int[] tamanyTaulell) {

		System.out.println("Comencem la partida...");
		int[][] taulell = taulellIni(tamanyTaulell);
		dibuixaPartida(taulell);

		int torn = 1;
		boolean win = false;
		int columna = 0, fila = 0;

		while (!win) {

			columna = triarColumna(torn, tamanyTaulell[1], taulell);
			fila = colocarFitxa(torn, columna, taulell);
			taulell[fila][columna] = torn;
			dibuixaPartida(taulell);

			win = comprovarLinia(torn, fila, columna, taulell);
			fiPartida(torn, win);
			torn = comprovarTorn(torn); // canvi de torn
		}
	}

	/**
	 * Seleccionar la columna i comprovar que està dins de la mida del taulell
	 * 
	 * @param torn:     per saber el jugador que juga
	 * @param columnes: quantitat total de columnes
	 * @param taulell:  on es juga
	 * 
	 * @return columna: per saber a quina columna, la retornem restan-li un , ja què
	 *         el jugador posarà nombres de l'1 al 7 o de l'1 a M segons hagi
	 *         seleccionat i així evitem problemes de rang i allà es colocarà la
	 *         fitxa
	 */
	public static int triarColumna(int torn, int columnes, int[][] taulell) {

		int columna = -1;
		while (columna <= 0 || columna > columnes) {
			System.out.println("Selecciona entre columna 1 i " + columnes);
			if (torn == 1) {
				System.out.println("En quina columna? " + jugador1);
				columna = sc.nextInt();

				// comprovem que està dins del rang del taulell
				if (columna <= 0 || columna > columnes) {
					System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + columnes);
				} else if (taulell[0][columna - 1] != 0) {
					System.out.println("Fila completa!!");
					columna = 0;
				}

			} else {

				System.out.println("En quina columna?" + jugador2);
				columna = sc.nextInt();

				// comprovem que està dins del rang del taulell
				if (columna <= 0 || columna > columnes) {
					System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + columnes);
				} else if (taulell[0][columna - 1] != 0) {
					System.out.println("Fila completa!!");
					columna = 0;
				}
			}
		}
		return columna - 1;
	}

	/**
	 * Col·locar la fitxa del jugador, la fitxa cau fins a la última fila, en la
	 * columna seleccionada si no està plena per una altra fitxa, les fitxes es
	 * corresponen amb el torn, jugador1 = 1 i jugador2 =2
	 * 
	 * @param torn:    per controlar el jugador i la fitxa que es col·loca
	 * 
	 * @param columna: per saber en quina columna
	 * 
	 * @param taulell: on es juga la partida
	 * 
	 * @return fila: on s'ha de colocar la fitxa
	 */
	public static int colocarFitxa(int torn, int columna, int[][] taulell) {
		int fila = 0;
		for (int i = taulell.length - 1; i >= 0; i--) {
			if (taulell[i][columna] == 0) {
				fila = i;
				break;
			}
		}
		return fila;

	}

	/**
	 * Funció controlar els torns
	 * 
	 * @param torn: per iniciar el torn o quin torn estava jugant
	 * 
	 * @return torn: torn canviat per continuar el joc
	 */
	public static int comprovarTorn(int torn) {
		return torn == 1 ? 2 : 1;
	}

	/**
	 * Funció per comprovar si hi ha línia en qualsevol sentit del taulell
	 * 
	 * @param torn:    per saber quin jugador
	 *
	 * @param fila:    files on es juga
	 * 
	 * @param columna: on es juga
	 * 
	 * @param taulell: on s'està jugant
	 * 
	 * @return boolean: per a poder cridar la següent comprovació a la funció
	 *         fiPartida()
	 */
	public static boolean comprovarLinia(int torn, int fila, int columna, int[][] taulell) {

		return (liniaHoritzontal(torn, fila, taulell) || liniaVertical(torn, columna, taulell)
				|| liniaDiagonal(torn, taulell));
	}

	/**
	 * Funció per comprovar si hi ha línies en horitzontal
	 * 
	 * @param torn:    per controlar qui juga
	 * 
	 * @param fila:    on s'ha col·locat la fitxa
	 * 
	 * @param taulell; on s'està jugant
	 * 
	 * @return boolean: per controlar si hi ha línia
	 * 
	 */
	public static boolean liniaHoritzontal(int torn, int fila, int[][] taulell) {
		int conn = 0;

		for (int i = 0; i < taulell[0].length; i++) {
			if (taulell[fila][i] == torn) {
				conn++;
			} else {
				if (conn >= 4) {
					return true;
				}
				conn = 0;
			}
		}
		System.out.println(conn);
		return conn >= 4;
	}

	/**
	 * Funció per comprovar si hi ha línies verticals
	 * 
	 * 
	 * @param torn:    per controlar qui juga
	 * 
	 * @param columna: on s'ha col·locat la fitxa
	 * 
	 * @param taulell: on s'està jugant
	 * 
	 * 
	 * @return boolean: per controlar si hi ha línia
	 */
	public static boolean liniaVertical(int torn, int columna, int[][] taulell) {
		int conn = 0;

		for (int i = 0; i < taulell.length; i++) {
			if (taulell[i][columna] == torn) {
				conn++;
			} else {
				if (conn >= 4) {
					return true;
				}
				conn = 0;
			}
		}
		return conn >= 4;
	}

	/**
	 * Funció per comprovar si hi ha línies en les diagonals
	 * 
	 * 
	 * @param torn:    per controlar qui juga
	 * 
	 * @param taulell: on es juga la partida
	 * 
	 * @return boolean: per controlar si hi ha línia
	 * 
	 */
	public static boolean liniaDiagonal(int torn, int[][] taulell) {

		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {

				// per comprovar que tenim suficient espai a vall
				if (i + 3 < taulell.length) {

					// comprovació cap a la dreta
					if (j + 3 < taulell[0].length) {
						int conn = 0;

						for (int k = i; k <= i + 3; k++) {
							if (taulell[k][j + (k - i)] == torn) {
								conn++;
							} else {
								conn = 0;
							}
						}
						if (conn == 4) {
							return true;
						}
					}

					// comprovació cap a l'esquerra
					if (j - 3 >= 0) {
						int conn = 0;

						for (int k = i; k <= i + 3; k++) {
							if (taulell[k][j - (k - i)] == torn) {
								conn++;
							} else {
								conn = 0;
							}
						}
						if (conn == 4) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Aquí comprovarem si hi ha guanyador de la partida, cal comprovar les files
	 * horitzontals, verticals i diagonals.
	 * 
	 * @param torn:  per saber quin jugador està tirant i fer les comprovacions de
	 *               guanyador
	 * 
	 * @param win: controlar qui guanya la partida
	 * 
	 */
	public static void fiPartida(int torn, boolean win) {
		if (win) {
			if (torn == 1) {
				System.out.println("A guanyat el jugador " + torn);
				if (classifList.containsKey(jugador1)) {
					classifList.put(jugador1, classifList.get(jugador1) + 1);
				} else {
					classifList.put(jugador1, 1);
				}
			} else if (torn == 2) {
				System.out.println("A guanyat el jugador " + torn);
				if (classifList.containsKey(jugador2)) {
					classifList.put(jugador2, classifList.get(jugador2) + 1);
				} else {
					classifList.put(jugador2, 1);
				}
			}
		}
	}

	/**
	 * Inicialitzar el taulell de joc a 0
	 * 
	 * @param tamanyTaulell: on es juga
	 * 
	 * @return taulell: taulell iniciat
	 * 
	 */
	public static int[][] taulellIni(int[] tamanyTaulell) {
		int[][] taulell = new int[tamanyTaulell[0]][tamanyTaulell[1]];
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				taulell[i][j] = 0;
			}
		}
		return taulell;
	}

	/**
	 * Per dibuixar el taulell a l'inici i durant la partida
	 * 
	 * @param taulell: el taulell que hi ha guardat en cada moment
	 * 
	 * 
	 */
	public static void dibuixaPartida(int[][] taulell) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");

	}

	/**
	 * Configuració del joc, triar jugadors i des d'aquí, seleccionar la mida del
	 * taulell
	 * 
	 *                 
	 * @return int[]: matriu per guardar la mida del taulell
	 */
	public static int[] configuracio() {

		sc.nextLine(); // per evitar errors de lectura
		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		jug = sc.nextInt();
		sc.nextLine();
		int[] tamanyTaulell = new int[2];
		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a"); // amb aquesta opció hi haurà la màquina
			jugador1 = sc.nextLine();
			System.out.println("jugador1 és " + jugador1);
			jugador2 = "maquina";
			System.out.println("jugador2 és " + jugador2);
			tamanyTaulell = midaTaulell(); // cridem la funció per seleccionar la mida del taulell

		} else {
			System.out.println("Nom dels/de les jugadors/es"); // jugaran dos humans...
			jugador1 = sc.nextLine();
			jugador2 = sc.nextLine();
			System.out.println("La partida es juga entre " + jugador1 + " i " + jugador2);
			tamanyTaulell = midaTaulell(); // cridem la funció per seleccionar la mida del taulell
		}
		return tamanyTaulell;
	}

	/**
	 * Seleccionar la mida del taulell, funció activada des de la configuració del
	 * joc.
	 * 
	 * @return int[]: per saber les mides per files i columnes
	 * 
	 */
	public static int[] midaTaulell() {
		int mida = 0, f = 0, c = 0; // variable local per a saber si vol modificar la mida del taulell
		sc.nextLine(); // netejar l'escaner per evitar errors.
		System.out.println("Tria la mida del taulell presionant qualsevol número, si no, presiona 0 i serà de 6 X 7");
		mida = sc.nextInt();
		sc.nextLine();
		if (mida != 0) {
			while (f < 4) {
				System.out.println("Quantitat de files, mínim 4");
				f = sc.nextInt();
				sc.nextLine();
			}
			while (c < 4) {
				System.out.println("Quantitat de columnes, mínim 4");
				c = sc.nextInt();
				sc.nextLine();
			}
			System.out.println("El taulell serà de " + f + " files " + c + " columnes.");

		} else {
			f = 6;
			c = 7;
			System.out.println("El taulell serà de " + f + " files " + c + " columnes.");

		}
		int[] tamanyTa = { f, c };
		return tamanyTa;
	}

	/**
	 * Per emmagatzemar els guanyadors i controlar-ne la posició
	 * 
	 */
	public static void ranking() {
		System.out.println("Classificació");
		for (Map.Entry<String, Integer> entry : classifList.entrySet()) {
			System.out.println(entry.getKey() + "::" + entry.getValue());
		}
	}

	/**
	 * Funicó per mostrar l'ajuda, s'intenta separar amb salts de linea (no s'ha
	 * aconseguit)
	 * 
	 */
	public static void mostrarajuda() {

		String salt = System.getProperty("line.separaor");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);
	}

	/**
	 * Funció sortir, per finalitzar el joc.
	 * 
	 */
	public static void sortir() {
		System.out.println("Pleguem!, Na Noch!!");
	}

}
