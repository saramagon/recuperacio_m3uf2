import java.util.ArrayList;
import java.util.Scanner;

/*
 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
 * caselles que inicialment està buit.
 * 
 * @author Sara-intentant-ho
 * 
 * @versio 8/06/20 4
 * 
 */

public class Connecta4_4 {
	/*
	 * l'enunciat demana no fer variables estàtiques. Un escaner per recollir tant
	 * les opcions com els noms és més lògic fer-lo static, per no haver d'anar
	 * declarant-lo cada cop que el necessitem. I les variables per a guardar els
	 * jugadors
	 */
	static Scanner sc = new Scanner(System.in);

	static String jugador1 = " ";
	static String jugador2 = " ";

	/*
	 * Variables globals per al nombre de files i columnes i el tauler per jugar
	 */
	static int f = 0;
	static int c = 0;
	static int[][] taulell = new int[f][c];

	public static void main(String[] args) {

		/* Una array per emmagatzemar els guanyadors */
		ArrayList<String> ordre = new ArrayList<String>();

		/* Declarem una variable per a la tria de l'opció */
		int op = -1;

		/* Variables per controlar el torn i si hi ha guanyador */
		int torn = 0;
		boolean win = false;

		/* Fem un bucle per a iniciar el joc */
		while (op != 0) {

			op = mostraMenu();
			int triar = sc.nextInt();
			switch (triar) {
			case 1:
				op = mostrarajuda();
				break;
			case 2:
				op = configuracio(taulell);
				break;
			case 3:
				op = jugar(torn, win, taulell);
				break;
			case 4:
				op = ranking();
				break;
			case 0:
				op = sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}
		}

	}

	/*
	 * Per jugar necessitem el tauler, cridem la funció per a dibuixar el tauler,
	 * amb el núm. columnes, núm. de files de les opcions del jugador. Comprovem si
	 * hi ha guanyador per finalitzar la partida. Tirada de les fitxes segons el
	 * torn de jugador.
	 * 
	 * @param torn: per a iniciar el joc amb el torn 1 i controlar els canvis de
	 * torn
	 * 
	 * @param win: per controlar el final de la partida
	 * 
	 * @param taulell2: on es juga la partida
	 * 
	 */
	private static int jugar(int torn, boolean win, int[][] taulell2) {
		taulellInici(taulell);
		dibuixaPartida(taulell);
		while (win) {
			System.out.println("Torn jugador " + torn);
			torn = tirarFitxa(torn);
			win = fiPartida(torn, taulell);
		}
		return 0;
	}

	/*
	 * Funció per a triar la fila on posar la fitxa controlant que està dins del
	 * rang de columnes del taulell, també es crida a la funció per col·locar-la on
	 * correspongui
	 * 
	 * @param torn: per controlar qui posa la fitxa
	 * 
	 * @return torn: per saber quin jugador tornarà a fer la col·locació
	 */
	private static int tirarFitxa(int torn) {

		sc.nextLine(); // netejar escaner per evitar errors.
		int columna = 0;
		boolean win = false;

		if (torn != 0) {
			if (torn == 1) {
				System.out.println("En quina columna? " + jugador1);
				columna = sc.nextInt();
				if (columna < 0 || columna >= c) { // comprovem que està dins del rang del taulell
					System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
				} else {
					colocarFitxa(torn, columna);
					torn = 2; //canvi de torn
					return torn;
				}
			}
		} else {
			System.out.println("En quina columna?" + jugador2);
			columna = sc.nextInt();
			if (columna < 0 || columna >= c) { // comprovem que està dins del rang del taulell
				System.out.println("Ordre de columna erroni, ha d'estar entre 0 i " + taulell.length);
			} else {
				colocarFitxa(torn, columna);
				torn = 1; //canvi de torn
				return torn;
			}
		}
		return torn;
	}

	/*
	 * Col·locar la fitxa del jugador, la fitxa cau fins a la última fila, en la
	 * columna seleccionada si no està plena per una altra fitxa, les fitxes es
	 * corresponen amb el torn, jugador1 = 1 i jugador2 =2
	 */
	private static void colocarFitxa(int torn, int col) {
		for (int i = taulell.length; i >= 0; i--) {
			if (taulell[i][col] != 0) {
				taulell[i - 1][col] = torn;
			}
		}
		dibuixaPartida(taulell);
	}

	/*
	 * Aquí comprovarem si hi ha guanyador de la partida, cal comprovar les files
	 * horitzontals, verticals i diagonals.
	 * 
	 * @return win: boleà per saber si hi ha guanyador i qui ha sigut
	 */
	private static boolean fiPartida(int torn, int[][] taulell2) {
		// variables per comptar les fitxes
		int conn = 0;

		// comprovar horitzontals
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell.length; j++) {
				if (taulell[i][j] == torn) {
					conn++;
				}
				if (conn == 4) {
					System.out.println("A guanyat el jugador " + torn);
					return true;
				}
			}
		}

		// comprovar verticals
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell.length; j++) {
				if (taulell[j][i] == torn) {
					conn++;
				}
				if (conn == 4) {
					System.out.println("A guanyat el jugador " + torn);
					return true;
				}
			}
		}

		// comprovar diagonals
		for (int i = 0; i < taulell.length; i++) {
			if (taulell[i][i] == torn) {
				conn++;
			} else if (taulell[i + 1][i] == torn) {
				conn++;
			} else if (taulell[i + 2][i] == torn) {
				conn++;
			} else if (taulell[i + 3][i] == torn) {
				conn++;
			} else if (taulell[i][i + 1] == torn) {
				conn++;
			} else if (taulell[i][i + 2] == torn) {
				conn++;
			} else if (taulell[i][i + 3] == torn) {
				conn++;
			} else if (taulell[i][i + 4] == torn) {
				conn++;
			}
			if (conn == 4) {
				System.out.println("El jugador " + torn + " a guanyat!!!!");
				return true;
			}
		}
		return false;
	}

	/*
	 * Des d'aquí s'haurà de poder definir la mida del tauler, si no es modifica
	 * serà l'estàndard de 6 X 7. S'ha de poder posar també nom als jugadors
	 * 
	 * @return op: per a continuar el joc
	 * 
	 * @return jugador1: Nom que anirà a la variable global corresponent.
	 * 
	 * @return jugador2: Nom que anirà a la variable global corresonent.
	 */
	private static int configuracio(int[][] taulell) {
		int op = -1;
		sc.nextLine(); // per evitar errors de lectura

		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		jug = sc.nextInt();
		sc.nextLine();

		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a"); // amb aquesta opció hi haurà la màquina
			jugador1 = sc.nextLine();
			System.out.println("jugador1 és " + jugador1);
			jugador2 = "maquina";
			System.out.println("jugador2 és " + jugador2);
			midaTaulell(); // cridem la funció per seleccionar la mida del taulell
			return op;

		} else {
			System.out.println("Nom dels/de les jugadors/es"); // jugaran dos humans...
			jugador1 = sc.nextLine();
			jugador2 = sc.nextLine();
			System.out.println("La partida es juga entre " + jugador1 + "i el " + jugador2);
			midaTaulell(); // cridem la funció per seleccionar la mida del taulell
			return op;
		}
	}

	/*
	 * Donar la opció de modificar la mida del taulell de joc, tot i que la funció
	 * és void retornarà les columnes, files i taulell, ja que les variables són
	 * globals i es modifiquen per cada crida que en fem d'elles.
	 */
	private static void midaTaulell() {
		int mida = 0; // variable local per a saber si vol modificar la mida del taulell
		sc.nextLine(); // netejar l'escaner per evitar errors.

		System.out.println("Presiona qualsevol número per tria la mida del taulell, si no, presiona 0 i serà de 6 X 7");
		mida = sc.nextInt();
		sc.nextLine();

		if (mida != 0) {
			System.out.println("Quantitat de columnes, mínim 4");
			c = sc.nextInt();
			sc.nextLine();
			System.out.println("Quantitat de columnes, mínim 4");
			f = sc.nextInt();
			sc.nextLine();
			System.out.println("El taulell serà de " + c + " columnes " + f + " files.");
		} else {
			c = 6;
			f = 7;
			System.out.println("El taulell serà de " + c + " columnes " + f + " files.");
		}
	}

	/*
	 * Dibuixar el tauler on s'ha de jugar inicialitzat a 0
	 * 
	 * @param fil: per al número de files triat o estàndard
	 * 
	 * @param col: per al número de columnes triat o estàndard
	 * 
	 * @param tauler: on es jugagarà, amb una mida de fil X col
	 */
	private static void taulellInici(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				taulell[i][j] = 0;
			}
		}
	}

	/*
	 * Per anar dibuixant l'evolució de la partida
	 * 
	 * @param taulell2: és el taulell amb les modificacions tant d'inicialització, o
	 * dels diferents moviments del jugadors
	 */
	public static void dibuixaPartida(int[][] taulell2) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");
	}

	/* Funicó per mostrar l'ajuda, s'intenta separar amb salts de linea */
	private static int mostrarajuda() {
		int op = -1;
		String salt = System.getProperty("line.separaor");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);
		return op;

	}

	/*
	 * Per mostrar menú amb les opcions que es poden seleccionar
	 * 
	 * @return op: és la selecció que accionarà cada opció
	 */
	private static int mostraMenu() {
		int op = 0;
		System.out.println("=======MENú=======");
		System.out.println("   1 - Ajuda");
		System.out.println("   2 - Opcions");
		System.out.println("   3 - Jugar partida");
		System.out.println("   4 - Veure rankings");
		System.out.println("   0 - Sortir");

		return op;
	}

	/* Per emmagatzemar els guanyadors i controlar-ne la posició */
	private static int ranking() {
		return -1;
		// TODO tot el que ha de fer..., controlar nom, puntuació i ordre ¿?

	}

	/*
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return -1, per fer sortir del bucle principal i finalitzar el joc
	 */
	private static int sortir() {
		System.out.println("Pleguem!, Na Noch!!");
		return 0;
	}
}
