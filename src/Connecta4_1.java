import java.util.ArrayList;
import java.util.Scanner;

public class Connecta4_1 {

	/*
	 * El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila
	 * continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM
	 * caselles que inicialment està buit.
	 * 
	 * @author Sara-intentant-ho
	 * 
	 * @versio 4/06/20 1
	 * 
	 */

	/*
	 * l'enunciat demana no fer variables estàtiques. Un escaner per recollir tant
	 * les opcions com els noms és més lògic fer-lo static, per no haver d'anar
	 * declarant-lo cada cop que el necessitem.
	 */
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		/*
		 * Variables per al nombre de files i columnes i el tauler per jugar
		 */
		int fil = 0;
		int col = 0;
		int[][] tauler = new int[fil][col];

		/* Una array per emmagatzemar els guanyadors */
		ArrayList<String> ordre = new ArrayList<String>();

		/* Declarem una variable per a la tria de l'opció */
		int op = 0;

		/* Fem un bucle per a iniciar el joc */
		while (op != -1) {

			op= mostraMenu();
			int triar = sc.nextInt();
			switch (triar) {
			case 1:
				mostrarajuda();
				break;
			case 2:
				configuracio();
				break;
			case 3:
				jugar();
				break;
			case 4:
				ranking();
				break;
			case 0:
				sortir();
				break;
			default:
				System.out.println("Torna-ho a provar");
				return;
			}
		}

	}

	/*
	 * Funció per a les opcions del menú sense paràmetres d'entrada
	 * 
	 * @return el valor de la opció triada per poder fer la sortida en cas de que ho
	 * sol·liciti el jugador
	
	private static int triaropcio() {
		int triar = sc.nextInt();
		switch (triar) {
		case 1:
			mostrarajuda();
			break;
		case 2:
			configuracio();
			break;
		case 3:
			jugar();
			break;
		case 4:
			ranking();
			break;
		case 0:
			sortir();
			break;
		default:
			System.out.println("Torna-ho a provar");
			return 6;
		}
		return triar;

	} */

	/*
	 * Funció sortir, per finalitzar el joc.
	 * 
	 * @return -1, per fer sortir del bucle principal i finalitzar el joc
	 */
	private static int sortir() {
		System.out.println("Pleguem!!");
		return -1;
	}

	private static void ranking() {
		// TODO Auto-generated method stub

	}

	/*
	 * Per jugar necessitem el tauler, cridem la funció per a dibuixar el tauler
	 */
	private static void jugar() {
		dibuixaTauler(0, 0, null);

	}

	/*
	 * Dibuixar el tauler on s'ha de jugar inicialitzat a 0
	 * 
	 * @param fil: per al número de files triat o estàndard
	 * 
	 * @param col: per al número de columnes triat o estàndard
	 * 
	 * @param tauler: on es jugagarà, amb una mida de fil X col
	 */
	private static void dibuixaTauler(int fil, int col, int[][] tauler) {
		for (int i = 0; i < fil; i++) {
			for (int j = 0; j < col; j++) {
				System.out.println(tauler[0][0]);
			}
		}
	}
	
	public static void dibuixaPartida(int[][] taulell) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}
		System.out.println("------------");
	}

	/*
	 * Des d'aquí s'haurà de poder definir la mida del tauler, si no es modifica
	 * serà l'estàndard de 6 X 7. S'ha de poder posar també nom als jugadors
	 */
	private static void configuracio() {
		sc.nextLine();//netejar l'escaner per evitar errors de guardat d'informació
		int jug = 0;
		System.out.println("Tria un/a jugador/a (1) o dos (2)");
		if (jug == 1) {
			j1 = seleccionarJuga();
		}
		j2 = seleccionarJuga();
		
	}

	
	private static String seleccionarJuga() {
	

		String jugador1;
		String jugador2;
		if (jug == 1) {
			System.out.println("Nom del/de la jugador/a 1");
			jugador1 = sc.nextLine();
			return jugador1;
		} if(jug==2){
			System.out.println("Nom del/de la jugador/a 2");
			jugador2 = sc.nextLine();
			return jugador2;
		}else 
			System.out.println("No has seleccionat res");
		return jugador1;
	}

	/* Funicó per mostrar l'ajuda, s'intenta separar amb salts de linea */
	private static void mostrarajuda() {
		String salt = System.getProperty("line.separaor");
		String ajuda = ("Aquest joc consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. "
				+ salt + "Es juga sobre un tauler de NxM caselles que inicialment està buit." + salt
				+ " Els dos jugadors situen les seves fitxes (una per moviment) al tauler. " + salt
				+ "La regla per col·locar-les consisteix que aquestes sempre cauen fins a baix. " + salt
				+ "És a dir una fitxa pot ser col·locada bé a la part inferior d'una columna o bé sobre una altra d'alguna altra columna.");
		System.out.print(ajuda);

	}

	private static int mostraMenu() {
		System.out.println("=======MENú=======");
		System.out.println("   1 - Ajuda");
		System.out.println("   2 - Opcions");
		System.out.println("   3 - Jugar partida");
		System.out.println("   4 - Veure rankings");
		System.out.println("   0 - Sortir");
	}

}
